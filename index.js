console.log("Hello World!");
//enclose with quotation mark string datas

console. log("Hello world!");
//case sensitive

console.
log
(
	"Hello, everyone"
	)

// ; delimiter
// we use delimiter to end our code

//[COMMENTS]
// - single line comments
//shortcut: ctrl + /

// I am a single line comment

/* Multi-line comment */
/*
	I am
	a
	multi line
	comment
*/
/* shortcut: ctrl + shift + / */

// Syntax and Statement

// statements in programming are instructions that we tell to computer to perform
// syntax in programming, it is the set of rules that describe how statements must be considered

// Variables

/*
	it is used to contain data

	-Syntax in declaring variables
	-let/const variableName
*/

let myVariable = "Hello";
			//assignment operator (=);

console.log(myVariable);

// console.log(hello); // will result to not defined error

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. For constant variable, use the 'const' keyword
		4. Variable names should be indicative or descriptive of the value being stored
		5. Never name a variable starting with numbers
		6. Refrain from using space in declaring a variable
*/


// String
let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

//Number
let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// Reassigning variable value
// Syntax
	// variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

// interest = 4.89;

const pi = 3.14;
// pi = 3.16;

// Reassigning - a variable already has a value and we reassign a new one
// Initialize - it is our first saving of a value

let supplier;
supplier  = "John Smith Trading"; // initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
// const let = "hello";
// console.log(let);

// [SECTION] Data Types

// Strings
// Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.
// Strings in Javascript is enclosed with single ('') or double ("") quote.

let country = 'Philippines';
let province = 'Metro Manila';

console.log(province + ', ' + country);
// We use + symbol to concatenate data / values
let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

// Escape Character (\)
// "\n" refers to creating a new line or set the text to next line;

console.log("line1\n\line2");

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);

message = 'John\'s employee went home early.';
console.log(message);

// Numbers

// Intergers / Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers/ float / fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);

// Arrays
// it stores multiple values with similar data type
let grades = [98.7, 95.4, 90.2, 94.6];
  // array Name     //elements
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that is used to mimic real world objects/items.
// Syntax:
/*
	let/const objectName = {
		propertyA: value,
		propertyB: value,
	}
*/

let person = {
	fullName: "Juan Dela Cruz", 
// key/property   //value
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 7444"],
	/*address: {
		houseNumber: "345",
		city: "Manila"
	}*/
};

console.log(person);

const myGrade = {
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading: 94.6,
};

console.log(myGrade);

let stringValue = "abcd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
//myGrades as object

// 'typeof'
// we use type of operator to retrieve / know the data type

console.log(typeof stringValue); //output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); //output: boolean
console.log(typeof waterBills); //output: object
console.log(typeof myGrade); //output: object

// Constant Objects and Arrays
// We cannot reassign the value of the variable, but we can change the elements of the constant array
const anime = ["Boruto", "One piece", "Code Geass", "Monster", "Dan Machi", "Demon Slayer", "AOT", "Fairy Tail"];
// 				0             1            2            3          4              5           6        7
// index - is the position of the element starting zero.

anime[0] = "Naruto";
console.log(anime);

// Null
// It is used to intentionally express the absence of a value
let spouse = null;
console.log(spouse);

// Undefined
// Represents the state of a variable that has been declared but without an assigned value
let fullName; //declaration
console.log(fullName);